package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class PanneauLateral extends JPanel {
	
	HashMap<Integer, PlayerInfoPanel> mapInfo;
	private WorldUI world;
	private int player;
	
	public PanneauLateral(WorldUI world, int player) {
		this.world = world;
		this.player = player;
		setPreferredSize(new Dimension(110, 320));
		mapInfo = new HashMap<>();
		initComponents();
	}
	
	private void initComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}
	
	public void setPlayer(int player) {
		this.player = player;
	}
	
	public void updatePlayerInfo(int player) {
		updatePlayerInfo(player, null);
	}
	
	public void updatePlayerInfo(int player, String msg) {
		SnakeUI snake = world.getSnake(player);
		PlayerInfoPanel pip;
		if (!mapInfo.containsKey(player)) {
			pip = new PlayerInfoPanel(snake.getName());
			mapInfo.put(player, pip);
			add(pip);
		} else {
			pip = mapInfo.get(player);
		}
		pip.setScore(snake.getScore());
		pip.setLives(snake.getLives());
		if (msg != null)
			pip.setMsg(msg);
	}
	
	public void updatePanel() {
		if (player != 0) {
			updatePlayerInfo(player);
		}
		for (SnakeUI snk : world.getSnakes()) {
			if (snk.getPlayer() != player) {
				updatePlayerInfo(snk.getPlayer());
			}
		}
	}
	
	private class PlayerInfoPanel extends JPanel {
		
		private JTextField scoreField;
		
		private JTextField livesField;
		
		private String playerName;
		
		private JLabel msgLabel;
		
		public PlayerInfoPanel(String playerName) {
			this.playerName = playerName;
			setPreferredSize(new Dimension(110, 50));
			setMaximumSize(new Dimension(110, 80));
			setMinimumSize(new Dimension(110, 50));
			initComponents();
		}
		
		private void initComponents() {
			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			setBorder(BorderFactory.createTitledBorder(playerName));
			scoreField = new JTextField(10);
			scoreField.setHorizontalAlignment(JTextField.RIGHT);
			scoreField.setEditable(false);
			livesField = new JTextField(2);
			livesField.setHorizontalAlignment(JTextField.RIGHT);
			livesField.setEditable(false);
			msgLabel = new JLabel();
			msgLabel.setForeground(Color.red);
			JPanel pnl = new JPanel(new GridLayout(0, 2, 2, 2));
			pnl.setPreferredSize(new Dimension(110, 50));
			pnl.setMaximumSize(new Dimension(110, 50));
			pnl.setMinimumSize(new Dimension(110, 50));
			pnl.add(new JLabel("Score"));
			pnl.add(scoreField);
			pnl.add(new JLabel("Lives"));
			pnl.add(livesField);
			add(pnl);
			add(Box.createGlue());
			add(msgLabel);
		}
		
		public void setScore(long score) {
			scoreField.setText(score + "");
		}
		
		public void setLives(int lives) {
			livesField.setText(lives + "");
		}
		
		public void setMsg(String msg) {
			msgLabel.setText(msg);
//			msgLabel.repaint();
		}
		
	}
	
}
