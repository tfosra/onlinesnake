package view;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.json.JSONObject;

import entity.AppleBlock;
import entity.IBlock;
import entity.PortalBlock;

@SuppressWarnings("serial")
public class BlockUI extends JPanel{

	public static final String IMG_PATH = "/img/";
	
	public static final String IMG_EXT = "png";
	
	public static final int WIDTH = 5;
	
	private IBlock block;
	
	private Image fond;
	
	public BlockUI() {
		
	}
	
	public BlockUI(IBlock block) {
		this.block = block;
		initBlock();
	}
	
	private void initBlock() {
		setBounds(block.getX() * WIDTH, block.getY() * WIDTH, WIDTH, WIDTH);
		String extra = "";
		if (block.getType() == IBlock.PORTAL_BLOCK) {
			extra = ((PortalBlock)block).getOrientation() + "";
		}
		if (block.getType() == IBlock.APPLE_BLOCK) {
			extra = ((AppleBlock)block).isSuperApple() ? "1" : "0";
		}
		String path = IMG_PATH + "block" 
							   + block.getType() + "" 
							   + block.getPlayer() + "" 
							   + extra + "." 
							   + IMG_EXT;
		fond = new ImageIcon(getClass().getResource(path)).getImage();
		repaint();
	}
	
	public IBlock getBlock() {
		return block;
	}
	
	public Image getFond() {
		return fond;
	}
	
	public void paintComponent(Graphics g){		
		g.drawImage(fond, 0, 0, this);
	}
	
	public void updateFromJSON(JSONObject json) {
		block.updateFromJSON(json);
		initBlock();
	}
	
}
