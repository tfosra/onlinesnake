package view;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import view.WorldUI.GamePanel;
import entity.Door;
import entity.Portal;

public class PortalUI {

	private Portal portal;
	
	private List<DoorUI> doors;
	
	public PortalUI(Portal portal) {
		this.portal = portal;
		doors = new ArrayList<>();
		for (Door d : portal.getDoors()) {
			doors.add(new DoorUI(d));
		}
	}
	
	public Portal getPortal() {
		return portal;
	}
	
	public List<DoorUI> getDoors() {
		return doors;
	}
	
	public void addToWorld(GamePanel world) {
		for (DoorUI d : doors) {
			d.addToWorld(world);
		}
	}
	
	public void updateFromJSON(JSONObject json) {
		portal.updateFromJSON(json);
		doors.clear();
		for (Door d : portal.getDoors()) {
			doors.add(new DoorUI(d));
		}
	}
	
}
