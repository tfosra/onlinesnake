package view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class CadreUI extends JPanel {

	private String bord_path = "/img/bordure.jpg";
	
	private Image imageBuffer;
	private Graphics bfg;
	
	public CadreUI() {
		super(null);
		setPreferredSize(new Dimension(320, 320));
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		if (imageBuffer != null) {
			g.drawImage(imageBuffer, 0, 0, null);
		}
	}
	
	private void renderBuffer() {
		if (imageBuffer == null) {
			Image bordure = new ImageIcon(getClass().getResource(bord_path)).getImage();
			imageBuffer = createImage(bordure.getWidth(this), bordure.getHeight(this));
			bfg = imageBuffer.getGraphics();
			bfg.drawImage(bordure, 0, 0, null);
		}
		repaint();
	}
	
	public void updateCadre() {
		renderBuffer();
	}
	
	public void display(Component c) {
		removeAll();
		repaint();
		add(c);
		revalidate();
	}
	
}
