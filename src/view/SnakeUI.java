package view;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import view.WorldUI.GamePanel;
import entity.ISnake;
import entity.SnakeBlock;

public class SnakeUI {

	private ISnake snake;
	
	private List<BlockUI> blocks;
	
	public SnakeUI(ISnake snake) {
		this.snake = snake;
		blocks = new ArrayList<>();
		for (SnakeBlock blk : snake.getBody()) {
			blocks.add(new BlockUI(blk));
		}
	}
	
	public ISnake getSnake() {
		return snake;
	}
	
	public List<BlockUI> getUIBlocks() {
		return blocks;
	}
	
	public String getName() {
		return this.snake.getName();
	}
	
	public void addToWorld(GamePanel world) {
		for (BlockUI blk : blocks) {
			world.add(blk);
		}
	}
	
	public void updateFromJSON(JSONObject json) {
		snake.updateFromJSON(json);
		blocks.clear();
		for (SnakeBlock blk : snake.getBody()) {
			blocks.add(new BlockUI(blk));
		}
	}
	
	public int getLives() {
		return snake.getLives();
	}
	
	public long getScore() {
		return snake.getScore();
	}
	
	public int getPlayer() {
		return snake.getPlayer();
	}
	
}
