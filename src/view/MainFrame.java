package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import network.Client;
import network.Serveur;
import observation.Observer;

import org.json.JSONObject;

import entity.AppleBlock;
import entity.ISnake;

@SuppressWarnings("serial")
public class MainFrame extends JFrame implements Observer {
	
	public final static String SOUND_APPLE_EAT = "/snd/apple_eat.wav";
	public final static String SOUND_NEW_SUPER_APPLE = "/snd/superapple_new.wav";
	public final static String SOUND_NEW_PORTAL = "/snd/portal_new.wav";
	
	private CadreUI cadre;
	
	private WorldUI world;
	
	private int player;
	
	private PanneauLateral lateralPanel;
	
	private WorldInfoPanel worldInfoPanel;
	
	private StatePanel statePanel;
	
	private Client client;
	
	private JButton startButton = new JButton("Start game");
	
	private JButton joinButton = new JButton("Join game");
	
	private JPanel discoveryServerPanel;
	
	private JPanel homePanel;

	private boolean gaming;
	
	public MainFrame() {
		setTitle("Snake Game v3.0");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		cadre = new CadreUI();
		world = new WorldUI();
		lateralPanel = new PanneauLateral(world, player);
		statePanel = new StatePanel();
		initComponents();
		pack();
	}
	
	private void startButtonAction(ActionEvent e) {
		new StartGameDialog().setVisible(true);
	}
	
	private void joinButtonAction(ActionEvent e) {
		resetDiscoveryPanel();
		String name = JOptionPane.showInputDialog("Pseudo");
		cadre.display(discoveryServerPanel);
		client = new Client(name);
		client.addObserver(this);
		client.startDiscovery();
	}
	
	private void initComponents() {
		homePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		homePanel.setBackground(Color.red);
		homePanel.setAlignmentY(Component.CENTER_ALIGNMENT);
		homePanel.setBounds(10, 10, 300, 300);
		homePanel.add(startButton);
		homePanel.add(joinButton);
		homePanel.setFocusable(false);
		startButton.addActionListener(event -> startButtonAction(event));
		joinButton.addActionListener(event -> joinButtonAction(event));
		getContentPane().add(cadre, "Center");
		getContentPane().add(lateralPanel, "East");
		getContentPane().add(statePanel, "South");
		world.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				treatKeyListener(e);
			}
		});
		discoveryServerPanel = new JPanel();
		discoveryServerPanel.setLayout(new BoxLayout(discoveryServerPanel, BoxLayout.Y_AXIS));
		discoveryServerPanel.setBounds(10, 10, 300, 300);
		worldInfoPanel = new WorldInfoPanel();
		worldInfoPanel.setBounds(10, 10, 300, 300);
		cadre.display(homePanel);
	}
	
	private void treatKeyListener(KeyEvent e) {
		if (!gaming)
			return;
		if (!Arrays.asList(KeyEvent.VK_SPACE,
						   KeyEvent.VK_LEFT,
						   KeyEvent.VK_UP,
						   KeyEvent.VK_RIGHT,
						   KeyEvent.VK_DOWN)
			  .contains(e.getKeyCode()))
				return;
		JSONObject json = new JSONObject();
		json.put("player", player);
		int direction = 0;
		switch (e.getKeyCode()){
			case KeyEvent.VK_SPACE:
				System.out.println("Sending pause");
				json.put("msg_type", "pause");
				break;
			case KeyEvent.VK_LEFT:
				direction = ISnake.LEFT;
				break;
			case KeyEvent.VK_UP:
				direction = ISnake.UP;
				break;
			case KeyEvent.VK_RIGHT:
				direction = ISnake.RIGHT;
				break;
			case KeyEvent.VK_DOWN:
				direction = ISnake.DOWN;
				break;
		}
		if (direction != 0) {
			json.put("msg_type", "direction");
			json.put("direction", direction);
		}
		sendMessage(json.toString());	
	}
	
	public void update(String msg) {
		JSONObject json = new JSONObject(msg);
		switch (json.getString("msg_type")) {
		case "notify_number":
			int number = json.getInt("number");
			this.player = number;
			lateralPanel.setPlayer(number);
			break;
		case "player_msg":
			int player = json.getInt("player");
			lateralPanel.updatePlayerInfo(player, json.optString("msg"));
			break;
		case "world_update":
			world.updateFromJSON(json);
			lateralPanel.updatePanel();
			if (!gaming)
				worldInfoPanel.updateInfo();
			world.updateWorld();
			break;
		case "start_game":
			gaming = true;
			cadre.display(world);
			world.displayWorld();
			world.updateWorld();
			statePanel.addElement("portal", "Portal", world.getWorld().getPortal().getDuration());
			break;
		case "timer":
			boolean startTimer = json.optBoolean("start_timer", false);
			if (startTimer) {
				cadre.display(world);
				world.displayNotification();
			}
			world.updateMessage(json.optString("msg"));
			break;
		case "pause":
			boolean pause = json.optBoolean("pause", false);
			if (pause) {
				String pauseMsg = "Pause";
				int pausedBy = json.optInt("player", 0);
				if (pausedBy != 0) {
					ISnake pausedBySnake = world.getWorld().getPlayer(pausedBy);
					if (pausedBySnake != null) {
						pauseMsg = "Paused by " + pausedBySnake.getName();
					}
				}
				cadre.display(world);
				world.displayNotification();
				world.updateMessage(pauseMsg);
			}
			else {
				cadre.display(world);
				world.displayWorld();
			}
			break;
		case "discovery_response":
			String name = json.getString("game_name");
			String address = json.getString("address");
			int players = json.getInt("game_size");
			int maxPlayers = json.getInt("game_limit");
			boolean available = json.optBoolean("available");
			System.out.println("Mainframe - New server discovered " + name + "@" + address);
			discoveryServerPanel.add(new ServerInfoPanel(name, address, players, maxPlayers, available));
			discoveryServerPanel.revalidate();
			break;
		case "game_over":
			gaming = false;
			int nbr = json.optInt("winner");
			ISnake winner = world.getWorld().getPlayer(nbr);
			cadre.display(world);
			world.displayNotification();
			String gm_msg = "Game Over - " + winner.getName() + " wins";
			world.updateMessage(gm_msg);
		case "portal":
			boolean new_portal = json.optBoolean("new_portal");
			if (new_portal) {
				AudioManager.play(SOUND_NEW_PORTAL);
			}
			int portal_timer = json.optInt("timer");
			statePanel.updateElement("portal", portal_timer);
			break;
		case "apple":
			boolean apple_eat = json.optBoolean("apple_eat");
			if (apple_eat) {
				AudioManager.play(SOUND_APPLE_EAT);
			}
			break;
		case "superapple":
			boolean new_superapple = json.optBoolean("new_superapple");
			if (new_superapple) {
				statePanel.addElement("superapple", "Super Apple", AppleBlock.DEFAULT_SUPERAPLE_DURATION);
				AudioManager.play(SOUND_NEW_SUPER_APPLE);
			}
			int superapple_timer = json.optInt("timer");
			statePanel.updateElement("superapple", superapple_timer);
			boolean remove_superapple = json.optBoolean("remove_superapple");
			if (remove_superapple) {
				statePanel.removeElement("superapple");
			}
			break;
		default:
			break;
		}
		
	}
	
	private void resetDiscoveryPanel() {
		discoveryServerPanel.removeAll();
		discoveryServerPanel.repaint();
	}
	
	public void startGame(String playerName, int gameType) {
		client = new Client(playerName);
		client.addObserver(this);
		new Serveur(gameType);
		client.joinGame("127.0.0.1", true);
		if (gameType == Serveur.MULTI_MODE) {
			cadre.display(worldInfoPanel);
		}
		else {
			client.startGame();
		}
		
	}
	
	public void sendMessage(String info) {
		client.sendMessage(info);
	}
	
	public void initiate() {
		cadre.updateCadre();
	}
	
	private class StartGameDialog extends JDialog {
		private JButton startBtn = new JButton("Start game");
		private JButton cancelBtn = new JButton("Cancel");
		
		private JTextField nameField;
		private JComboBox<String> modeCombo;
		
		public StartGameDialog() {
			super(MainFrame.this, true);
			setSize(250, 150);
			setLocationRelativeTo(null);
			initComponents();
		}
		
		private void initComponents() {
			setLayout(new BorderLayout());
			JPanel centerPanel = new JPanel(new BorderLayout());
			JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
			JPanel p1 = new JPanel(new GridLayout(0, 1, 0, 5));
			JPanel p2 = new JPanel(new GridLayout(0, 1, 0, 5));
			p1.add(new JLabel("Player name"));
			p1.add(new JLabel("Game type"));
			nameField = new JTextField();
			modeCombo = new JComboBox<>(new String[]{"Single game", "Multiplayer game"});
			p2.add(nameField);
			p2.add(modeCombo);
			centerPanel.add(p1, "West");
			centerPanel.add(p2, "Center");
			southPanel.add(startBtn);
			southPanel.add(cancelBtn);
			add(centerPanel, "Center");
			add(southPanel, "South");
			cancelBtn.addActionListener(event -> this.dispose());
			startBtn.addActionListener(event -> {
				startGame(nameField.getText(), modeCombo.getSelectedIndex());
				dispose();
			});
		}
	}
	
	private class WorldInfoPanel extends JPanel {
		
		private JLabel nameLabel;
		private JPanel playersPanel;
		private JLabel waitLabel;
		private JButton startButton;
		
		public WorldInfoPanel() {
			setPreferredSize(new Dimension(300, 300));
			initComponents();
		}
		
		private void initComponents() {
			setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
			nameLabel = new JLabel();
			playersPanel = new JPanel();
			playersPanel.setSize(new Dimension(300, 20));
			playersPanel.setMinimumSize(playersPanel.getSize());
			playersPanel.setMaximumSize(new Dimension(300, 150));
			playersPanel.setLayout(new BoxLayout(playersPanel, BoxLayout.Y_AXIS));
			playersPanel.setBorder(BorderFactory.createTitledBorder("Players"));
			waitLabel = new JLabel();
			startButton = new JButton("Start game");
			startButton.addActionListener(event -> client.startGame());
			add(nameLabel);
			add(playersPanel);
			add(waitLabel);
			add(Box.createGlue());
			add(startButton, BorderLayout.PAGE_END);
			updateInfo();
		}
		
		public void updateInfo() {
			if (world.getWorld() == null)
				return;
			String snkName = "";
			ISnake snk = world.getCreatedBySnake();
			if (snk != null)
				snkName = snk.getName();
			nameLabel.setText("Created by : " + snkName);
			waitLabel.setText("Waiting players (" + world.getWorld().getSize() + "/" + world.getWorld().getLimit() + ")");
			playersPanel.removeAll();
			playersPanel.repaint();
			world.getWorld()
				.getSnakes()
				.forEach(sk -> playersPanel.add(new JLabel(sk.getPlayer() + " - " + sk.getName())));
			playersPanel.revalidate();
			boolean isCreator = false;
			try {
				isCreator = world.getWorld().getCreatedBy() == player;
			} catch (Exception e) {}
			startButton.setVisible(isCreator);
			repaint();
		}
		
	}
	
	private class ServerInfoPanel extends JPanel {
		
		private String address;
		private int players;
		private int maxPlayers;
		private String name;
		private boolean available;
		private JButton joinButton;
		
		public ServerInfoPanel(String name, String address, int players, int maxPlayers, boolean available) {
			this.name = name;
			this.address = address;
			this.players = players;
			this.maxPlayers = maxPlayers;
			this.available = available;
			initComponents();
		}
		
		private void initComponents() {
			setPreferredSize(new Dimension(300, 40));
			setMaximumSize(new Dimension(300, 40));
			setMinimumSize(new Dimension(300, 40));
			setBorder(new EtchedBorder());
			setLayout(new BorderLayout(2, 1));
			add(new JLabel(name + "@" + address), "Center");
			add(new JLabel(players + "/" + maxPlayers), "South");
			joinButton = new JButton("Join");
			joinButton.setEnabled(available);
			joinButton.setAlignmentY(Component.CENTER_ALIGNMENT);
			add(joinButton, "East");
			joinButton.addActionListener(event -> {
				client.joinGame(address);
				cadre.display(worldInfoPanel);
			});
		}
		
	}
	
	public static void main(String args[]) {
		MainFrame mf = new MainFrame();
		mf.setVisible(true);
		mf.initiate();
	}

}
