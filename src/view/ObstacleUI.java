package view;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import view.WorldUI.GamePanel;
import entity.IBlock;
import entity.Obstacle;

public class ObstacleUI {
	
	private Obstacle obstacle;
	
	private List<BlockUI> blocks;
	
	public ObstacleUI(Obstacle obstacle) {
		this.obstacle = obstacle;
		blocks = new ArrayList<>();
	}
	
	public Obstacle getObstacle() {
		return obstacle;
	}
	
	public List<BlockUI> getUIBlocks() {
		return blocks;
	}
	
	public void addToWorld(GamePanel world) {
		for (BlockUI blk : blocks) {
			world.add(blk);
		}
	}
	
	public void updateFromJSON(JSONObject json) {
		obstacle = new Obstacle();
		obstacle.updateFromJSON(json);
		blocks.clear();
		for (IBlock blk : obstacle.getBlocks()) {
			blocks.add(new BlockUI(blk));
		}
	}

}
