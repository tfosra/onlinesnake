package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

@SuppressWarnings("serial")
public class StatePanel extends JPanel {

	private HashMap<String, StatePanelElement> elements;
	
	public StatePanel() {
		elements = new HashMap<>();
		initComponents();
	}
	
	private void initComponents() {
		setPreferredSize(new Dimension(300, 45));
		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
		setBorder(BorderFactory.createEtchedBorder());
	}
	
	public void addElement(String key, String title, int maxValue) {
		StatePanelElement elt = new StatePanelElement(title, maxValue); 
		elements.put(key, elt);
		add(elt);
	}

	public void removeElement(String key) {
		StatePanelElement elt = elements.get(key);
		if (elt != null) {
			remove(elt);
			revalidate();
			repaint();
			elements.remove(key);
		}
	}
	
	public void updateElement(String key, int value) {
		StatePanelElement elt = elements.get(key);
		if (elt != null) {
			elt.updateProgress(value);
		}
	}
	
	class StatePanelElement extends JPanel {
		private int maxValue;
		private String title;
		private JProgressBar progressBar;
		
		public StatePanelElement(String title, int maxValue) {
			this.title = title;
			this.maxValue = maxValue;
			initComponents();
		}
		
		private void initComponents() {
			setLayout(new BorderLayout());
//			setPreferredSize(new Dimension(60, 30));
			setBorder(BorderFactory.createTitledBorder(title));
			progressBar = new JProgressBar(0, maxValue - 1);
			add(progressBar);
		}
		
		public void updateProgress(int value) {
			progressBar.setValue(value);
		}
	}
	
}
