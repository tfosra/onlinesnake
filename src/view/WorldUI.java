package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import org.json.JSONObject;

import entity.IBlock;
import entity.ISnake;
import entity.World;

@SuppressWarnings("serial")
public class WorldUI extends JLayeredPane {

	private BlockUI apple;
	
	private PortalUI portal;
	
	private List<SnakeUI> snakes;
	
	private ObstacleUI obstacle;
	
	private World world;
	
	protected NotificationPanel notificationPanel;
	
	protected GamePanel gamePanel;
	
	public WorldUI() {
		this(null);
	}
	
	public WorldUI(World world) {
		super();
		this.world = world;
		initWorld();
		setSize(new Dimension(300, 300));
		setPreferredSize(getSize());
		setBounds(10, 10, getWidth(), getHeight());
		setFocusable(true);
		initComponents();
	}
	
	private void initComponents() {
		gamePanel = new GamePanel(getWidth(), getHeight());
		notificationPanel = new NotificationPanel(getWidth(), getHeight());
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				requestFocus();
			}
		});
		add(gamePanel);
		add(notificationPanel);
	}
	
	private void initWorld() {
		snakes = new ArrayList<>();
		if (world == null)
			return;
		for (ISnake snk : world.getSnakes()) {
			snakes.add(new SnakeUI(snk));
		}
		apple = null;
		if (world.getApple() != null) {
			apple = new BlockUI(world.getApple());
		}
		obstacle = null;
		if (world.getObstacle() != null) {
			obstacle = new ObstacleUI(world.getObstacle());
		}
		portal = null;
		if (world.getPortal() != null) {
			portal = new PortalUI(world.getPortal());
		}
	}
	
	public BlockUI getApple() {
		return apple;
	}
	
	public void updateApple(IBlock block) {
		this.apple = new BlockUI(block);
	}
	
	public ObstacleUI getObstacle() {
		return obstacle;
	}
	
	public void setObstacle(ObstacleUI obstacle) {
		this.obstacle = obstacle;
	}
	
	public void addSnake(SnakeUI snake) {
		snakes.add(snake);
	}
	
	public List<SnakeUI> getSnakes() {
		return snakes;
	}
	
	public SnakeUI getSnake(int player) {
		return snakes.get(player - 1);
	}
	
	public void updateWorld() {
		gamePanel.updateWorld();
	}
	
	public World getWorld() {
		return world;
	}
	
	public ISnake getCreatedBySnake() {
		if (world != null) {
			return world.getCreatedBySnake();
		}
		return null;
	}
	
	public void displayWorld() {
//		notificationPanel.setVisible(false);
		moveToFront(gamePanel);
		requestFocus();
	}
	
	public void displayNotification() {
//		notificationPanel.setVisible(true);
		moveToFront(notificationPanel);
		requestFocus();
	}
	
	public void updateFromJSON(JSONObject json) {
		if (world == null) {
			world = new World();
		}
		world.updateFromJSON(json);
		initWorld();
	}
	
	public void updateMessage(String msg) {
		notificationPanel.updateMessage(msg);
		repaint();
	}
	
	class GamePanel extends JPanel {
		
		private Image imageBuffer;
		
		private Graphics bfg;
		
		public GamePanel(int width, int height) {
			super(null);
			setSize(width, height);
			setPreferredSize(getSize());
		}
		
		public void updateWorld() {
			removeAll();
			repaint();
			renderBuffer();
		}
		
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			if (imageBuffer != null) {
				g.drawImage(imageBuffer, 0, 0, null);
			}
		}
		
		private void renderBuffer() {
			if (imageBuffer == null) {
				int width = BlockUI.WIDTH * world.getxSize();
				int height = BlockUI.WIDTH * world.getySize();
				Image cadre = new ImageIcon(getClass().getResource("/img/cadre.jpg")).getImage();
				imageBuffer = createImage(width, height);
				if (imageBuffer != null) {
					bfg = imageBuffer.getGraphics();
					bfg.drawImage(cadre, 0, 0, null);
				}
			}
			for (SnakeUI snk : snakes) {
				snk.addToWorld(this);
			}
			if (obstacle != null) {
				obstacle.addToWorld(this);
			}
			if (apple != null) {
				this.add(apple);
			}
			if (portal != null) {
				portal.addToWorld(this);
			}
			revalidate();
		}
		
	}
	
	class NotificationPanel extends JPanel {

		private JLabel lbl;
		
		public NotificationPanel(int width, int height) {
			super(new GridLayout(1, 1));
			setSize(width, height);
			initComponents();
		}
		
		private void initComponents() {
			setBackground(new Color(255, 255, 255, 190));
			lbl = new JLabel();
			lbl.setFont(new Font("Arial", Font.BOLD, 18));
			lbl.setHorizontalAlignment(JLabel.CENTER);
			add(lbl);
		}
		
		public void updateMessage(String msg) {
			lbl.setText(msg);
			repaint();
		}
		
	}
	
}
