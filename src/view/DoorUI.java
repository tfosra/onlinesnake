package view;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import view.WorldUI.GamePanel;
import entity.Door;
import entity.IBlock;

public class DoorUI {

	private Door door;
	private List<BlockUI> blocks;
	
	public DoorUI(Door door) {
		this.door = door;
		blocks = new ArrayList<>();
		for (IBlock blk : door.getBlocks()) {
			blocks.add(new BlockUI(blk));
		}
	}
	
	public void addToWorld(GamePanel world) {
		for (BlockUI blk : blocks) {
			world.add(blk);
		}
	}
	
	public void updateFromJSON(JSONObject json) {
		door.updateFromJSON(json);
		blocks.clear();
		for (IBlock blk : door.getBlocks()) {
			blocks.add(new BlockUI(blk));
		}
	}
	
}
