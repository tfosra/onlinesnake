package view;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

public class AudioManager {

	public static void play(String audiopath) {
		new Thread(){
			public void run() {
				try {
					AudioInputStream ais = AudioSystem.getAudioInputStream(getClass().getResource(audiopath));
					AudioFormat format = ais.getFormat();
					DataLine.Info info = new DataLine.Info(Clip.class, format);
					Clip clip = (Clip) AudioSystem.getLine(info);
					clip.open(ais);
					clip.start();
//					while (clip.isRunning()) {
//						Thread.sleep(10);
//					}
					Thread.sleep(20);
					clip.drain();
					clip.stop();
					clip.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			};
		}.start();
	}
	
}
