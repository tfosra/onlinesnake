package network;

import org.json.JSONObject;

import entity.AppleBlock;
import entity.ISnake;
import entity.World;

public class Game extends Thread {

	private World world;
	private boolean pause;
	private boolean running;
	private Serveur server;
	private boolean superappleReached;
	
	public Game(Serveur server) {
		this(server, World.DEFAULT_MAX_PLAYERS);
	}
	
	public Game(Serveur server, int maxPlayers) {
		this(server, maxPlayers, 0);
	}
	
	public Game(Serveur server, int maxPlayers, int createdBy) {
		world = new World(maxPlayers);
		world.setCreatedBy(createdBy);
		this.server = server;
	}
	
	public int getCreatedBy() {
		return world.getCreatedBy();
	}
	
	public String getMainPlayerName() {
		ISnake snk = world.getCreatedBySnake();
		if (snk != null) {
			return snk.getName();
		}
		return "";
	}
	
	public int getSize() {
		return world.getSnakes().size();
	}
	
	public int getLimit() {
		return world.getLimit();
	}
	
	public synchronized void setPause(boolean pause) {
		this.pause = pause;
		if (!pause) 
			notifyAll();
	}
	
	public boolean getPause() {
		return this.pause;
	}
	
	public void startGame() {
		if (!running) {
			world.updateApple();
			this.start();
		}
	}
	
	public void stopGame() {
		this.running = false;
	}
	
	public void togglePause() {
		
	}
	
	public World getWorld() {
		return world;
	}
	
	public void updateGame(String msg) {
		JSONObject json = new JSONObject(msg);
		switch (json.getString("msg_type")) {
		case "pause":
			togglePause();
			break;
		case "direction":
			world.updateDirection(json.getInt("player"), json.getInt("direction"));
		default:
			break;
		}
	}
	
	public int addPlayer(String name) {
		return world.addPlayer(name);
	}
	
	public boolean available() {
		return !world.isFull() && !running;
	}
	
	public void manageDeadSnakes(Integer[] snakes) {
		for (int player : snakes) {
			ISnake snk = world.getPlayer(player);
			if (snk.canLive()) {
				new Thread(){
					public void run() {
						playerCountDown(snk.getPlayer());
						snk.setAlive(true);
					};
				}.start();
			}
			else {
				JSONObject json = new JSONObject();
				json.put("msg_type", "player_msg");
				json.put("player", player);
				json.put("msg", "Game over");
				server.sendMessage(json.toString());
			}
		}
	}
	
	public void playerCountDown(int player) {
		JSONObject json = new JSONObject();
		json.put("msg_type", "player_msg");
		json.put("player", player);
		for (int t = Serveur.PLAYER_TIMER; t > 0; t--) {
			json.put("msg", t);
			server.sendMessage(json.toString());
			try {
				sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		json.put("msg", "");
		server.sendMessage(json.toString());
	}
	
	public ISnake getWinner() {
		return world.getSnakes().stream()
				.sorted((snk2, snk1) -> Long.compare(snk1.getScore(), snk2.getScore()))
				.findFirst()
				.orElse(null);
	}
	
	public void randomizePortal() {
		int timer = 0;
		while (running && !world.isGameOver()) {
			//Tant que le jeu est en pause on reste bloqué
			synchronized (this) {
				while (pause) {
					try {
						wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			JSONObject json = new JSONObject();
			json.put("msg_type", "portal");
			
			if (timer == 0) {
				world.updatePortal();

				// Notify the players about the genaration of a new portal (typically for a sound notification)
				json.put("new_portal", true);
			}
			// Notify the players about the portal timer countdown
			json.put("timer", timer);
			server.sendMessage(json.toString());
			
			timer++;
			timer %= world.getPortal().getDuration();
			try {
				sleep(1000); // sleep for 1s
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void countDownSuperApple() {
		int timer = 0;
		superappleReached = false;
		while (!superappleReached && running && !world.isGameOver() && timer < AppleBlock.DEFAULT_SUPERAPLE_DURATION) {
			//Tant que le jeu est en pause on reste bloqué
			synchronized (this) {
				while (pause) {
					try {
						wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			JSONObject json = new JSONObject();
			json.put("msg_type", "superapple");
			if (timer == 0) {
				// Notify the players about the genaration of a new superapple (typically for a sound notification)
				json.put("new_superapple", true);
			}
			// Notify the players about the superapple timer countdown
			json.put("timer", timer);
			server.sendMessage(json.toString());

			timer++;
			try {
				sleep(1000); // sleep for 1s
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		JSONObject json = new JSONObject();
		json.put("msg_type", "superapple");
		json.put("remove_superapple", true);
		server.sendMessage(json.toString());
	}
	
	private int manageApple(int apples_eat) {
		synchronized (world) {
			if (world.checkAppleReached()) {
				// Check if the reached apple is a superapple
				if (world.getApple().isSuperApple()) {
					superappleReached = true;
				}
				else {
					apples_eat++;
				}
				// Notify the players about an apple which has been ate
				JSONObject json = new JSONObject();
				json.put("msg_type", "apple");
				json.put("apple_eat", true);
				server.sendMessage(json.toString());
				
				boolean generateSuperApple = apples_eat == World.DEFAULT_SUPERAPPLE_CYCLE; 
				world.updateApple(generateSuperApple);
				apples_eat %= World.DEFAULT_SUPERAPPLE_CYCLE;

				// Start the superapple countdown
				if (generateSuperApple) {
					new Thread() {
						public void run() {
							countDownSuperApple();
							if (!superappleReached && running && !world.isGameOver()) {
								world.updateApple();
								server.notifyWorld();
							}
						};
					}.start();
				}
			}
			return apples_eat;
		}
	}
	
	public void run() {
		running = true;
		new Thread(){
			public void run() {
				randomizePortal();
			};
		}.start();
		int apples_eat = 0;
		while (running && !world.isGameOver()) {
			//Tant que le jeu est en pause on reste bloqué
			synchronized (this) {
				while (pause) {
					try {
						wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			world.moveSnakes();
			manageDeadSnakes(world.checkSnakeDead());
			apples_eat = manageApple(apples_eat);
			server.notifyWorld();
			try {
				sleep(world.getSpeed());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (world.isGameOver()) {
			server.notifyGameOver();
		}
		else if (!running) {
//			JSONObject json = new JSONObject();
//			json.put("msg_type", value)
		}
		running = false;
	}
	
}
