package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import entity.ISnake;
import entity.World;

public class Serveur {
	
	public final static int SERVER_UDP_PORT = 2299;
	
	public final static int DISCOVERY_PORT = 3399;
	
	public final static int SINGLE_MODE = 0;
	
	public final static int MULTI_MODE = 1;
	
	public final static int TIMER = 3;
	
	public final static int PLAYER_TIMER = 3;
		
	private boolean discoveryActive;
	
	private boolean gaming;
	
	private Game game;
	
	private HashMap<Integer, InetAddress> players;

	private int gameMode;

	public Serveur() {
		this(SINGLE_MODE);
	}
	
	public Serveur(int gameMode) {
		this.gameMode = gameMode;
		players = new HashMap<>();
		startGame();
		startDiscovery();
	}
	
	public void startGame() {
		if (!gaming) {
			gaming = true;
			int maxP = World.DEFAULT_MAX_PLAYERS;
			if (gameMode == SINGLE_MODE)
				maxP = 1;
			game = new Game(this, maxP);
			new GamingThread().start();
		}
	}
	
	public void stopGame() {
		gaming = false;
	}
	
	public void startDiscovery() {
		if (!discoveryActive) {
			System.out.println("Server - Server discovery started");
			discoveryActive = true;
			new DiscoveryThread().start();
		}
	}
	
	public void stopDiscovery() {
		discoveryActive = false;
	}
	
	private class DiscoveryThread extends Thread {
		@Override
		public void run() {
			try {
				// Keep a socket open to listen to all the UDP trafic that is destined for this port
				String address = "127.0.0.1";
				if (gameMode == MULTI_MODE)
					address = "0.0.0.0";
				DatagramSocket socket = new DatagramSocket(DISCOVERY_PORT, InetAddress.getByName(address));
				socket.setBroadcast(true);
				while (discoveryActive) {
					// Receive a packet
					byte[] recvBuf = new byte[15000];
					DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
					socket.receive(packet);

					// See if the packet holds the right command (message)
					String msg = new String(packet.getData()).trim();
					try {
						JSONObject json = new JSONObject(msg);
						switch (json.getString("msg_type")) {
						case "discovery_request":
							System.out.println("New discovery request");
							json = new JSONObject();
							json.put("msg_type", "discovery_response");
							json.put("game_name", game.getMainPlayerName());
							json.put("game_size", game.getSize());
							json.put("game_limit", game.getLimit());
							json.put("available", game.available());
							// Send a response
							byte[] sendData = json.toString().getBytes();
							DatagramPacket sendPacket = new DatagramPacket(
									sendData, sendData.length, packet.getAddress(),
									packet.getPort());
							socket.send(sendPacket);
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}
				socket.close();
			} catch (IOException ex) {
				Logger.getLogger(DiscoveryThread.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}
	}
	
	private class GamingThread extends Thread {
		
		DatagramSocket socket;
		
		@Override
		public void run() {
			try {
				System.out.println("Server - Starting gaming thread");
				// Keep a socket open to listen to all the UDP trafic that is destined for this port
				socket = new DatagramSocket(SERVER_UDP_PORT, InetAddress.getByName("0.0.0.0"));
				while (gaming) {
					// Receive a packet
					byte[] recvBuf = new byte[15000];
					DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
					socket.receive(packet);

					// See if the packet holds the right command (message)
					String msg = new String(packet.getData()).trim();
					try {
						JSONObject json = new JSONObject(msg);
						switch (json.getString("msg_type")) {
						case "join_game":
							treatJoinGame(json, packet);
							break;
						case "pause":
							treatPause(json, packet);
							break;
						case "start_game":
							treatStartGame(json, packet);
							break;
						case "direction":
							treatDirection(json, packet);
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}
				socket.close();
			} catch (IOException ex) {
				Logger.getLogger(DiscoveryThread.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}
		
		private void treatStartGame(JSONObject json, DatagramPacket packet) throws Exception {
			new Thread() {
				@Override
				public void run() {
					countDown();
					JSONObject json = new JSONObject();
					json.put("msg_type", "start_game");
					sendMessage(json.toString());
					game.startGame();
				}
			}.start();
		}
		
		private void countDown() {
			JSONObject json = new JSONObject();
			json.put("msg_type", "timer");
			json.put("start_timer", true);
			sendMessage(json.toString());
			for (int t = TIMER; t > 0; t--) {
				json = new JSONObject();
				json.put("msg_type", "timer");
				json.put("msg", t);
				sendMessage(json.toString());
				try {
					sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		private void treatJoinGame(JSONObject json, DatagramPacket packet) throws Exception {
			String playerName = json.getString("player_name");
			System.out.println("Server - Player trying to join the game " + playerName);
			int number = game.addPlayer(playerName);
			players.put(number, packet.getAddress());
			if (json.optBoolean("creator")) {
				game.getWorld().setCreatedBy(number);
			}
			// Notify player with his number
			JSONObject msg = new JSONObject();
			msg.put("msg_type", "notify_number");
			msg.put("number", number);
			sendMessage(msg.toString(), number);
			notifyWorld();
		}
		
		private void treatPause(JSONObject json, DatagramPacket packet) throws Exception {
			boolean pause = !game.getPause();
			json.put("pause", pause);
			new Thread() {
				public void run() {
					if (!pause)
						countDown();
					game.setPause(pause);
					sendMessage(json.toString());
				}
			}.start();
		}
		
		private void treatDirection(JSONObject json, DatagramPacket packet) throws Exception {
			game.updateGame(json.toString());
		}
		
	}
	
	public void notifyWorld() {
		JSONObject json = new JSONObject(game.getWorld().toString());
		json.put("msg_type", "world_update");
		sendMessage(json.toString());
	}
	
	public void notifyGameOver() {
		JSONObject json = new JSONObject(game.getWorld().toString());
		json.put("msg_type", "game_over");
		ISnake winner = game.getWinner();
		json.put("winner", winner == null ? 0 : winner.getPlayer());
		sendMessage(json.toString());
	}
	
	void sendMessage(String msg) {
		players.forEach((player, address) -> sendMessage(msg, player));
	}
	
	void sendMessage(String msg, int player) {
		try {
			DatagramSocket clientSocket = new DatagramSocket();
			InetAddress address = players.get(player);
			byte[] sendData = new byte[15000];
			sendData = msg.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, address, Client.CLIENT_UDP_PORT);
			clientSocket.send(sendPacket);
			clientSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]) {
		new Serveur(MULTI_MODE);
	}
	
}
