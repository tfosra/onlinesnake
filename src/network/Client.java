package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import observation.Observable;
import observation.Observer;

import org.json.JSONObject;

public class Client implements Observable {
	
	public final static int CLIENT_UDP_PORT = 1199;
	
	private ArrayList<Observer> listObservers = new ArrayList<>();
	
	private boolean gaming = true;
	
	private boolean discoveryActive;

	private String pseudo;
	
	private InetAddress serverAddress;
	
	public Client(String pseudo) {
		this.pseudo = pseudo;
	}
	
	public void startDiscovery() {
		if (!discoveryActive) {
			discoveryActive = true;
			new DiscoveryThread().start();
		}
	}
	
	public void stopDiscovery() {
		discoveryActive = false;
	}
	
	private class DiscoveryThread extends Thread {
		
		public void run() {
			// Find the servers using UDP broadcast
			try {
				// Open a random port to send the package
				DatagramSocket c = new DatagramSocket();
				c.setBroadcast(true);

				JSONObject json = new JSONObject();
				json.put("msg_type", "discovery_request");
				byte[] sendData = json.toString().getBytes();

				// Try the 255.255.255.255 first
				try {
					DatagramPacket sendPacket = new DatagramPacket(sendData,
							sendData.length,
							InetAddress.getByName("255.255.255.255"), Serveur.DISCOVERY_PORT);
					c.send(sendPacket);
				} catch (Exception e) {
					e.printStackTrace();
				}

				// Broadcast the message over all the network interfaces
				Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
				while (interfaces.hasMoreElements()) {
					NetworkInterface networkInterface = interfaces.nextElement();
					if (networkInterface.isLoopback() || !networkInterface.isUp()) {
						continue; // Don't want to broadcast to the loopback interface
					}
					for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
						InetAddress broadcast = interfaceAddress.getBroadcast();
						if (broadcast == null) {
							continue;
						}
						// Send the broadcast package!
						try {
							DatagramPacket sendPacket = new DatagramPacket(
									sendData, sendData.length, broadcast, Serveur.DISCOVERY_PORT);
							c.send(sendPacket);
						} catch (Exception e) {
						}
					}
				}
				while (discoveryActive) {
					// Wait for a response
					byte[] recvBuf = new byte[15000];
					DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
					c.receive(receivePacket);
	
					// Check if the message is correct
					String msg = new String(receivePacket.getData()).trim();
					try {
						json = new JSONObject(msg);
						switch (json.getString("msg_type")) {
						case "discovery_response":
							System.out.println("Client - New discovery request response from " + json.getString("game_name"));
							json.put("address", receivePacket.getAddress().getHostAddress());
							notifyObservers(json.toString());
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}
				c.close();
			} catch (IOException ex) {
				Logger.getLogger(Client.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		}
		
	}
	
	private class GamingThread extends Thread {
		
		DatagramSocket socket;
		
		@Override
		public void run() {
			try {
				// Keep a socket open to listen to all the UDP trafic that is destined for this port
				socket = new DatagramSocket(CLIENT_UDP_PORT);
				while (gaming) {
					// Receive a packet
					byte[] recvBuf = new byte[15000];
					DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
					socket.receive(packet);

					// See if the packet holds the right command (message)
					String msg = new String(packet.getData()).trim();
					try {
//						JSONObject json = new JSONObject(msg);
						notifyObservers(msg);
//						switch (json.getString("msg_type")) {
//						case "discovery_request":
//							
//							break;
//						}
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}
				socket.close();
			} catch (IOException ex) {
				Logger.getLogger(DiscoveryThread.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}	
	}
	
	public boolean joinGame(String server) {
		return joinGame(server, false);
	}
	
	public boolean joinGame(String server, boolean creator) {
		try {
			System.out.println("Client - Trying to join server game at " + server);
			new GamingThread().start();
			this.serverAddress = InetAddress.getByName(server);
			JSONObject json = new JSONObject();
			json.put("msg_type", "join_game");
			json.put("player_name", pseudo);
			json.put("creator", creator);
			sendMessage(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean startGame() {
		JSONObject json = new JSONObject();
		json.put("msg_type", "start_game");
		sendMessage(json.toString());
		return true;
	}
	
	public void sendMessage(String msg) {
		try {
			DatagramSocket clientSocket = new DatagramSocket();
			byte[] sendData = msg.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, serverAddress, Serveur.SERVER_UDP_PORT);
			clientSocket.send(sendPacket);
			clientSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getPseudo() {
		return pseudo;
	}

	public void addObserver(Observer obs) {
		listObservers.add(obs);
	}

	public void removeObservers() {
		listObservers.clear();
	}

	public void notifyObservers(String message) {
		for (Observer obs : listObservers)
			obs.update(message);
	}
	
}
