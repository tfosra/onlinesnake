package entity;

import java.awt.Point;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

public class Door {
	private LinkedList<IBlock> blocks;
	private int direction;
	private int size;
	private Portal portal;
	
	public Door(Portal portal) {
		this.portal = portal;
	}
	
	public Door(Portal portal, Point origin, int size, int direction) {
		this.portal = portal;
		blocks = new LinkedList<>();
		this.size = size;
		this.setDirection(direction);
		initDoor(origin);
	}
	
	private void initDoor(Point origin) {
		int x = origin.x;
		int y = origin.y;
		int xDir = Math.abs(this.getDirection()) % 2;
		int yDir = Math.abs(this.getDirection()) / 2;
		IBlock b;
		for (int i = 0; i < size; i++) {
			b = new PortalBlock(new Point(x + i * xDir, y + i * yDir), direction + 2);
			blocks.add(b);
		}
	}
	
	public LinkedList<IBlock> getBlocks() {
		return blocks;
	}
	
	public int hasCrossed(Point p, int direction) {
		synchronized (portal) {
			if (Math.abs(direction) != Math.abs(getDirection())) {
				return -1;
			}
			int xDir = -1 * direction / 2;
			int yDir = -1 * direction % 2;
			IBlock b = new Block(p.x + xDir, p.y + yDir, 0);
			return blocks.indexOf(b);
		}
	}
	
	public int canCross(Point p, int direction) {
		synchronized (portal) {
			if (getDirection() == direction) {
				return blocks.indexOf(new Block(p, 0));
			}
			if (getDirection() == -1 * direction ) {
				int xDir = direction / 2;
				int yDir = direction % 2;
				IBlock b = new Block(p.x + xDir, p.y + yDir, 0);
				return blocks.indexOf(b);
			}
			return -1;
		}
	}
	
	public Point transformPoint(int position, boolean followPortalDirection) {
		synchronized (portal) {
			IBlock portalBlock = blocks.get(position);
			Point p = new Point(portalBlock.getPoint());
			if (followPortalDirection) {
				int coef = followPortalDirection ? 1 : -1;
				int xDir = coef * this.getDirection() / 2;
				int yDir = coef * this.getDirection() % 2;
				p.translate(xDir, yDir);
			}
			return p;
		}
	}
	
	public boolean isDoorOnObject() {
		boolean res = false;
		for (IBlock b : blocks) {
			res = res || portal.getWorld().isBlockOnObject(b);
		}
		return res;
	}
	
	public boolean crossesDoor(Door d) {
		synchronized (portal) {
			if (d == null)
				return false;
			for (IBlock b : blocks) {
				if (d.blocks.contains(b))
					return true;
			}
			return false;
		}
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}
	
	public boolean insideBounds() {
		boolean res = true;
		for (IBlock blk : blocks) {
			res = res && portal.getWorld().insideBounds(blk.getPoint());
		}
		return res;
	}
	
	public boolean distantFrom(Door d) {
		int minX1 = blocks.stream().min((b1, b2) -> Integer.compare(b1.getX(), b2.getX())).get().getX();
		int minX2 = d.blocks.stream().min((b1, b2) -> Integer.compare(b1.getX(), b2.getX())).get().getX();
		int minY1 = blocks.stream().min((b1, b2) -> Integer.compare(b1.getY(), b2.getY())).get().getY();
		int minY2 = d.blocks.stream().min((b1, b2) -> Integer.compare(b1.getY(), b2.getY())).get().getY();
		int maxX1 = blocks.stream().max((b1, b2) -> Integer.compare(b1.getX(), b2.getX())).get().getX();
		int maxX2 = d.blocks.stream().max((b1, b2) -> Integer.compare(b1.getX(), b2.getX())).get().getX();
		int maxY1 = blocks.stream().max((b1, b2) -> Integer.compare(b1.getY(), b2.getY())).get().getY();
		int maxY2 = d.blocks.stream().max((b1, b2) -> Integer.compare(b1.getY(), b2.getY())).get().getY();
		int xWidth = Math.max(maxX1, maxX2) - Math.min(minX1, minX2);
		int yWidth = Math.max(maxY1, maxY2) - Math.min(minY1, minY2);
		return (xWidth + yWidth) >= size * 2;
	}
	
	@Override
	public String toString() {
		JSONObject json = new JSONObject();
		json.put("direction", direction);
		json.put("size", size);
		JSONArray array = new JSONArray();
		for (IBlock blk : blocks) {
			array.put(new JSONObject(blk.toString()));
		}
		json.put("blocks", array);
		return json.toString();
	}
	
	public void updateFromJSON(JSONObject json) {
		this.direction = json.getInt("direction");
		this.size = json.getInt("size");
		JSONArray arrayBlock = json.getJSONArray("blocks");
		blocks = new LinkedList<>();
		IBlock blk;
		for (int i = 0; i < arrayBlock.length(); i++) {
			blk = new PortalBlock();
			blk.updateFromJSON(arrayBlock.getJSONObject(i));
			blocks.add(blk);
		}
	}
	
}
