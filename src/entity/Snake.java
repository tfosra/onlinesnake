package entity;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Snake implements ISnake {

	private LinkedList<SnakeBlock> body;
		
	private String name;
	
	private int direction;
	
	private int player;
	
	private int lives;
	
	private long score;
	
	private boolean alive;
	
	public Snake() {
		
	}
	
	public Snake(String name, int player, int direction) {
		this(name, player, direction, 0, 0);
	}
	
	public Snake(String name, int player, int direction, int x, int y) {
		this.name = name;
		this.direction = direction;
		this.player = player;
		this.lives = DEFAULT_LIVES;
		this.score = 0;
		this.alive = true;
		initSnake(x, y);
	}
	
	/**
	 * Initialise et positionne le snake sur la grille puis éventuellement informe les autres joueurs sur le r�seau (cas du jeu en réseau)
	 */
	public void initSnake(int x, int y) {
		this.body = new LinkedList<>();
		int xDir = this.direction / 2;
		int yDir = this.direction % 2;
		SnakeBlock sb;
		for (int i = 0; i < (DEFAULT_SIZE - 1); i++) {
			sb = new SnakeBlock(x + i * xDir, y + i * yDir, this.player);
			body.addFirst(sb);
		}
		sb = new SnakeBlock(x + (DEFAULT_SIZE - 1) * xDir , y + (DEFAULT_SIZE - 1) * yDir, this.player);
		sb.setType(IBlock.HEAD_BLOCK);
		body.addFirst(sb);
	}
	
	public void resetSnake() {
		this.direction = World.getDefaultDirection(getPlayer());
		this.alive = false;
	}
	
	public void grow() {
		SnakeBlock tail = body.peekLast();
		SnakeBlock blk = new SnakeBlock(tail.getX(), tail.getY(), this.player);
		body.add(blk);
	}
	
	/**
	 * @return la tête du snake c'est à dire le block principal
	 */
	public IBlock getHead() {
		return body.peekFirst();
	}
	
	public LinkedList<SnakeBlock> getBody() {
		return body;
	}
	
	@Override
	public void setBody(LinkedList<SnakeBlock> body) {
		this.body = body;
	}
	
	@Override
	public void setScore(long score) {
		this.score = score;
	}
	
	@Override
	public void updateScore(long score) {
		this.score += score;
	}
	
	/**
	 * Updates the direction of the snake
	 * @return true if it is possible and false if not
	 */
	public boolean updateDirection(int direction) {
		if (this.direction == -direction) return false;
		this.direction = direction;
		return true;
	}
	
	/**
	 * Verifies if a point is on the snake
	 */
	public synchronized boolean isOnSnake(IBlock block) {
		if ((block.getPlayer() == getPlayer()) && (block.equals(getHead()))) {
			List<SnakeBlock> sublist = body.subList(1, body.size());
			return sublist.contains(block);
		}
		return body.contains(block);
	}
	
	public long getScore() {		
		return score;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDirection() {
		return direction;
	}
	
	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}
	
	public boolean isAlive() {
		return this.alive;
	}
	
	@Override
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	
	public boolean canLive() {
		return getLives() > 0;
	}

	public void moveSnake(int maxX, int maxY){
		SnakeBlock blk = body.getFirst();
		blk.setType(IBlock.BODY_BLOCK);
		int headX = blk.getX();
		int headY = blk.getY();
		int xDir = this.direction / 2;
		int yDir = this.direction % 2;
		headX = (headX + xDir + maxX)%maxX;
		headY = (headY + yDir + maxY)%maxY;
		blk = body.pollLast();
		blk.setType(IBlock.HEAD_BLOCK);
		blk.setX(headX);
		blk.setY(headY);
		body.addFirst(blk);
	}
	
	public boolean crossPortal(Portal portal) {
		synchronized (portal) {
			Point head_point = body.getFirst().getPoint();
			Point p = portal.transformPoint(head_point, direction);
			if (p != null) {
				setDirection(portal.transformDirection(head_point, direction));
				body.getFirst().setType(IBlock.BODY_BLOCK);
				SnakeBlock head = body.pollLast();
				head.setType(IBlock.HEAD_BLOCK);
				head.setX(p.x);
				head.setY(p.y);
				body.addFirst(head);
				return true;
			}
			return false;
		}
	}
	
	public void killSnake() {
		resetSnake();
		lives--;
	}
	
	@Override
	public int getLength() {
		return body.size();
	}
	
	@Override
	public String toString() {
		JSONObject json = new JSONObject();
		json.put("name", name);
		json.put("direction", direction);
		json.put("lives", lives);
		json.put("score", score);
		json.put("alive", alive);
		json.put("player", getPlayer());
		JSONArray array = new JSONArray();
		for (IBlock blk : body) {
			array.put(new JSONObject(blk.toString()));
		}
		json.put("body", array);
		return json.toString();
	}
	
	@Override
	public void updateFromJSON(JSONObject json) {
		this.name = json.getString("name");
		this.direction = json.getInt("direction");
		this.lives = json.getInt("lives");
		this.score = json.getLong("score");
		this.alive = json.optBoolean("alive");
		this.player = json.getInt("player");
		JSONArray arrayBlock = json.getJSONArray("body");
		body = new LinkedList<>();
		SnakeBlock blk;
		for (int i = 0; i < arrayBlock.length(); i++) {
			blk = new SnakeBlock();
			blk.updateFromJSON(arrayBlock.getJSONObject(i));
			body.add(blk);
		}
	}

}
