package entity;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

public class World {
	
	private final static int DEFAULT_XSIZE = 60;
	private final static int DEFAULT_YSIZE = 60;
	public final static int DEFAULT_MAX_PLAYERS = 4;
	public final static int DEFAULT_APPLE_SCORE = 75;
	public final static int DEFAULT_SUPERAPPLE_SCORE = 500;
	public final static int DEFAULT_SUPERAPPLE_CYCLE = 3; // 1 new superapple after 3 apples eat
	private final static int[] DIRECTIONS = {ISnake.RIGHT, ISnake.DOWN, ISnake.LEFT, ISnake.UP};
	
	private int xSize;
	private int ySize;
	private int speed;
	private int maxPlayers;
	
	private Obstacle obstacle;
	private IBlock apple;
	private ArrayList<ISnake> snakes;
	private Portal portal;
	
	private int createdBy;
	
	public World() {
		this(DEFAULT_MAX_PLAYERS);
	}
	
	public World(int maxPlayers) {
		this(maxPlayers, DEFAULT_XSIZE, DEFAULT_YSIZE);
	}
	
	public World(int maxPlayers, int xSize, int ySize) {
		this.maxPlayers = Math.min(maxPlayers, DEFAULT_MAX_PLAYERS);
		this.xSize = xSize;
		this.ySize = ySize;
		snakes = new ArrayList<>();
		this.speed = 200;
		this.portal = new Portal(this, 5);
	}
	
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	
	public int getCreatedBy() {
		return createdBy;
	}
	
	public ISnake getCreatedBySnake() {
		return getPlayer(getCreatedBy());
	}
	
	public int getLimit() {
		return maxPlayers;
	}
	
	public int getxSize() {
		return xSize;
	}
	
	public int getySize() {
		return ySize;
	}
	
	public void setObstacle(Obstacle obstacle) {
		this.obstacle = obstacle;
	}

	public Obstacle getObstacle() {
		return obstacle;
	}

	public Portal getPortal() {
		return portal;
	}
	
	public void setPortal(Portal portal) {
		this.portal = portal;
	}
	
	public AppleBlock getApple() {
		return (AppleBlock) apple;
	}
	
	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public ArrayList<ISnake> getSnakes() {
		return snakes;
	}

	public int addPlayer(String name) {
		int pNum = snakes.size() + 1;
		int dir = getDefaultDirection(pNum); 
		int xPos = getInitialX(pNum);
		int yPos = getInitialY(pNum);
		ISnake snk = new Snake(name, pNum, dir, xPos, yPos);
		return addPlayer(snk);
	}
	
	public int addPlayer(ISnake snk) {
		snakes.add(snk);
		return snakes.size();
	}
	
	public ISnake getPlayer(int number) {
		try {
			return snakes.get(number - 1);
		} catch (Exception e) {
			return null;
		}
	}
	
	public int getSize() {
		return snakes.size();
	}
	
	public boolean isFull() {
		return getSize() >= maxPlayers;
	}
	
	public void updateApple() {
		updateApple(false);
	}
	
	public void updateApple(boolean isSuperApple) {
		IBlock apl;
		do {
			apl = new AppleBlock(getRandomPoint(), isSuperApple);
		} while (isBlockOnObject(apl));
		this.apple = apl;
	}
	
	public void updatePortal() {
		portal.resetPortal();
	}
	
	public boolean isBlockOnObject(IBlock block) {
		boolean res = false;
		for (ISnake snk : snakes) {
			if (block.getType() != IBlock.APPLE_BLOCK && !snk.isAlive())
				continue;
			res = res || snk.isOnSnake(block);
		}
		if (obstacle != null) {
			res = res || obstacle.isOnObstacle(block);
		}
		return res;
	}
	
	public Point getRandomPoint() {
		int x = new Random().nextInt(xSize);
		int y = new Random().nextInt(ySize);
		return new Point(x, y);
	}
	
	public int getRandomDirection() {
		int pos = new Random().nextInt(DIRECTIONS.length);
		return DIRECTIONS[pos];
	}
	
	public boolean isAppleReached(ISnake snk) {
		IBlock head = snk.getHead();
		return head.equals(apple);
	}
	
	public boolean checkAppleReached() {
		for (ISnake snk : snakes) {
			if (isAppleReached(snk)) {
				snk.grow();
				snk.updateScore(getAppleScore());
				return true;
			}
		}
		return false;
	}
	
	public void updateDirection(int player, int direction) {
		ISnake snk = snakes.get(player - 1);
		snk.updateDirection(direction);
	}
	
	public void moveSnakes() {
		for (ISnake snk : snakes) {
			if (!snk.isAlive())
				continue;
			if (snk.crossPortal(portal))
				continue;
			snk.moveSnake(xSize, ySize);
		}
	}
	
	public boolean checkSnakeDead(ISnake snk) {
		return isBlockOnObject(snk.getHead());
	}
	
	public Integer[] checkSnakeDead() {
		ArrayList<Integer> lst = new ArrayList<>(getSize());
		for (ISnake snk : snakes) {
			if (!snk.canLive())
				continue;
			if (checkSnakeDead(snk)) {
				snk.killSnake();
				snk.initSnake(getInitialX(snk.getPlayer()), getInitialY(snk.getPlayer()));
				lst.add(snk.getPlayer());
			}
		}
		Integer[] res = new Integer[lst.size()];
		return lst.toArray(res);
	}
	
	public void resetGame() {
		for (ISnake snk : snakes) {
			int number = snk.getPlayer();
			((Snake)snk).setDirection(getDefaultDirection(number));
			snk.initSnake(getInitialX(number), getInitialY(number));
		}
	}
	
	public boolean isGameOver() {
		for (ISnake snk : snakes) {
			if (snk.canLive())
				return false;
		}
		return true;
	}
	
	public static int getDefaultDirection(int playerNumber) {
		return DIRECTIONS[playerNumber - 1];
	}
	
	public int getInitialX(int playerNumber) {
		int[] xPos = {0, xSize - 1, xSize - 1, 0};
		return xPos[playerNumber - 1];
	}
	
	public int getInitialY(int playerNumber) {
		int[] yPos = {0, 0, ySize - 1, ySize - 1};
		return yPos[playerNumber - 1];
	}
	
	public long getAppleScore() {
		AppleBlock a = (AppleBlock)apple;
		if (a.isSuperApple())
			return DEFAULT_SUPERAPPLE_SCORE;
		return DEFAULT_APPLE_SCORE;
	}
	
	public boolean insideBounds(Point p) {
		return p.x >= 0 &&
				p.x < xSize &&
				p.y >= 0 &&
				p.y < ySize;
	}
	
	@Override
	public String toString() {
		JSONObject json = new JSONObject();
		json.put("xSize", xSize);
		json.put("ySize", ySize);
		json.put("speed", speed);
		json.put("created_by", createdBy);
		if (apple != null) {
			json.put("apple", apple.toString());
		}
		if (obstacle != null) {
			json.put("obstacle", obstacle.toString());
		}
		if (portal != null) {
			json.put("portal", portal.toString());
		}
		JSONArray array = new JSONArray();
		for (ISnake snk : snakes) {
			array.put(new JSONObject(snk.toString()));
		}
		json.put("snakes", array);
		return json.toString();
	}
	
	public void updateFromJSON(JSONObject json) {
		obstacle = null;
		if (json.optString("obstacle", null) != null) {
			obstacle = new Obstacle();
			obstacle.updateFromJSON(new JSONObject(json.getString("obstacle")));
		}
		portal = null;
		if (json.optString("portal", null) != null) {
			portal = new Portal();
			portal.updateFromJSON(new JSONObject(json.getString("portal")));
		}
		this.xSize = json.getInt("xSize");
		this.ySize = json.getInt("ySize");
		this.speed = json.getInt("speed");
		this.createdBy = json.getInt("created_by");
		apple = null;
		if (json.optString("apple", null) != null) {
			apple = new AppleBlock();
			apple.updateFromJSON(new JSONObject(json.getString("apple")));
		}
		JSONArray arraySnake = json.getJSONArray("snakes");
		ISnake snk;
		snakes = new ArrayList<>();
		for (int i = 0; i < arraySnake.length(); i++) {
			snk = new Snake();
			snk.updateFromJSON(arraySnake.getJSONObject(i));
			snakes.add(snk);
		}
	}
	
}
