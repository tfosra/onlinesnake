package entity;

import org.json.JSONObject;

public class SnakeBlock extends Block {

	protected int player;
	
	public SnakeBlock() {
		
	}
	
	public SnakeBlock(int x, int y, int player) {
		super(x, y, BODY_BLOCK);
		this.player = player;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}
	
	@Override
	public void updateFromJSON(JSONObject json) {
		super.updateFromJSON(json);
		this.player = json.getInt("player");
	}
	
}
