package entity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Obstacle {

	private List<IBlock> blocks;
	
	public Obstacle() {
		this.blocks = new ArrayList<>();
	}
	
	public void addBlock(IBlock blk) {
		blocks.add(blk);
	}
	
	public List<IBlock> getBlocks() {
		return this.blocks;
	}
	
	public void setBlocks(List<IBlock> blocks) {
		this.blocks = blocks;
	}
	
	public boolean isOnObstacle(IBlock blk) {
		return blocks.contains(blk);
	}
	
	@Override
	public String toString() {
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		for (IBlock blk : blocks) {
			array.put(new JSONObject(blk.toString()));
		}
		json.put("blocks", array);
		return json.toString();
	}
	
	public void updateFromJSON(JSONObject json) {
		JSONArray arrayBlock = json.getJSONArray("blocks");
		blocks = new ArrayList<>();
		IBlock blk;
		for (int i = 0; i < arrayBlock.length(); i++) {
			blk = new Block();
			blk.updateFromJSON(arrayBlock.getJSONObject(i));
			blocks.add(blk);
		}
	}
	
}
