package entity;

import java.awt.Point;

import org.json.JSONObject;


public class Block implements IBlock {

	protected int type;
	
	protected Point point;
	
	public Block() {
		
	}
	
	public Block(int x, int y, int type) {
		this(new Point(x, y), type);
	}
	
	public Block(Point p, int type) {
		this.point = p;
		this.type = type;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getX() {
		return point.x;
	}

	public void setX(int x) {
		this.point.x = x;
	}

	public int getY() {
		return point.y;
	}

	public void setY(int y) {
		this.point.y = y;
	}

	@Override
	public Point getPoint() {
		return this.point;
	}
	
	@Override
	public boolean equals(Object blk) {
		if (blk == null || !(blk instanceof IBlock)) {
			return false;
		}
		Block b = (Block)blk;
		return this.point.equals(b.point);
	}
	
	@Override
	public int getPlayer() {
		return 0;
	}
	
	public String toString() {
		 JSONObject json = new JSONObject();
		 json.put("x", point.x);
		 json.put("y", point.y);
		 json.put("type", type);
		 json.put("player", getPlayer());
		 return json.toString();
	}
	
	@Override
	public void updateFromJSON(JSONObject json) {
		int x = json.getInt("x");
		int y = json.getInt("y");
		this.point = new Point(x, y);
		this.type = json.getInt("type");
	}
	
}
