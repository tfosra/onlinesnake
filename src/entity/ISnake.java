package entity;

import java.util.LinkedList;

import org.json.JSONObject;

public interface ISnake {

	public static final int UP = -1;
	
	public static final int DOWN = 1;
	
	public static final int LEFT = -2;
	
	public static final int RIGHT = 2;
	
	public static final int DEFAULT_SIZE = 3;
	
	public static final int DEFAULT_LIVES = 4;
	
	public boolean updateDirection(int direction);
	
	public boolean isOnSnake(IBlock block);
	
	public int getLength();
	
	public IBlock getHead();
	
	public void setScore(long score);
	
	public long getScore();
	
	public void updateScore(long score);
	
	public void moveSnake(int maxX, int maxY);
	
	public boolean crossPortal(Portal portal);
	
	public void initSnake(int x, int y);
	
	public String getName();
	
	public void setName(String name);
	
	public void setLives(int lives);
	
	public int getLives();
	
	public void setAlive(boolean alive);
	
	public boolean isAlive();
	
	public LinkedList<SnakeBlock> getBody();
	
	public void setBody(LinkedList<SnakeBlock> body);
	
	public void updateFromJSON(JSONObject json);
	
	public int getPlayer();
	
	public void killSnake();
	
	public void grow();
	
	public boolean canLive();
	
}
