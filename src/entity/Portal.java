package entity;

import java.awt.Point;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

public class Portal {
	
	public final static int DEFAULT_SIZE = 3;
	
	public final static int DEFAULT_DURATION = 15; // 15s 
	
	private LinkedList<Door> doors;
	private World world;
	private int size;
	private int duration;
	
	public Portal() {
		
	}
	
	public Portal(World world) {
		this(world, DEFAULT_SIZE);
	}
	
	public Portal(World world, int size) {
		this(world, size, DEFAULT_DURATION);
	}
	
	public Portal(World world, int size, int duration) {
		this.world = world;
		this.size = size;
		this.duration = duration;
		doors = new LinkedList<>();
	}
	
	public synchronized void resetPortal() {
		boolean notPlaced = true;
		Door d1 = null;
		Door d2 = null;
		doors.clear();
		while (notPlaced) {
			d1 = new Door(this, world.getRandomPoint(), size, world.getRandomDirection());
			d2 = new Door(this, world.getRandomPoint(), size, world.getRandomDirection());
			notPlaced = d1.isDoorOnObject() || d2.isDoorOnObject() || d1.crossesDoor(d2);
			notPlaced = notPlaced || !d1.insideBounds() || !d2.insideBounds();
		}
		doors.add(d1);
		doors.add(d2);
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public World getWorld() {
		return world;
	}
	
	public LinkedList<Door> getDoors() {
		return doors;
	}
	
	public synchronized Point transformPoint(Point p, int direction) {
		int crossPosition = -1;
		for (Door d : doors) {
			crossPosition = d.canCross(p, direction);
			if (crossPosition != -1) {
				Door exitDoor = getExitDoor(d);
				return exitDoor.transformPoint(crossPosition, direction==d.getDirection());
			}
		}
		return null;
	}
	
	public synchronized int transformDirection(Point p, int direction) {
		int crossPosition = -1;
		for (Door d : doors) {
			crossPosition = d.canCross(p, direction);
			if (crossPosition != -1) {
				int coef = direction == d.getDirection() ? 1 : -1;
				Door exitDoor = getExitDoor(d);
				return coef * exitDoor.getDirection();
			}
		}
		return direction;
	}
	
	private Door getExitDoor(Door crossedDoor) {
		int entryIndex = doors.indexOf(crossedDoor);
		int exitIndex = (entryIndex + 1) % doors.size();
		return doors.get(exitIndex);
	}
	
	@Override
	public String toString() {
		JSONObject json = new JSONObject();
		json.put("size", size);
		json.put("duration", duration);
		JSONArray array = new JSONArray();
		for (Door d : doors) {
			array.put(new JSONObject(d.toString()));
		}
		json.put("doors", array);
		return json.toString();
	}
	
	public void updateFromJSON(JSONObject json) {
		this.size = json.getInt("size");
		this.duration = json.getInt("duration");
		JSONArray arrayBlock = json.getJSONArray("doors");
		doors = new LinkedList<>();
		Door d;
		for (int i = 0; i < arrayBlock.length(); i++) {
			d = new Door(this);
			d.updateFromJSON(arrayBlock.getJSONObject(i));
			doors.add(d);
		}
	}

}
