package entity;

import java.awt.Point;

import org.json.JSONObject;

public interface IBlock {

	public final static int HEAD_BLOCK = 1;
	
	public final static int BODY_BLOCK = 2;
	
	public final static int APPLE_BLOCK = 3;
	
	public final static int WALL_BLOCK = 4;
	
	public final static int PORTAL_BLOCK = 5;
	
	public final static int ORIENTATION_L = 0;
	
	public final static int ORIENTATION_U = 1;
	
	public final static int ORIENTATION_D = 3;
	
	public final static int ORIENTATION_R = 4;
	
//	public final static String IMG_PATH = "/img/";
	
	public int getX();
	
	public int getY();
	
	public Point getPoint();
	
	public int getType();
	
	public int getPlayer();
	
	public void updateFromJSON(JSONObject json);
	
}
