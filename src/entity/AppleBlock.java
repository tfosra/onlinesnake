package entity;

import java.awt.Point;

import org.json.JSONObject;

public class AppleBlock extends Block {

	private boolean isSuperApple;
	
	public final static int DEFAULT_SUPERAPLE_DURATION = 10; // 10s
	
	public AppleBlock() {
		super();
	}
	
	public AppleBlock(Point p) {
		this(p, false);
	}
	
	public AppleBlock(Point p, boolean isSuperApple) {
		super(p, IBlock.APPLE_BLOCK);
		this.isSuperApple = isSuperApple;
	}

	public boolean isSuperApple() {
		return isSuperApple;
	}

	public void setSuperApple(boolean isSuperApple) {
		this.isSuperApple = isSuperApple;
	}
	
	public String toString() {
		String res = super.toString();
		JSONObject json = new JSONObject(res);
		json.put("superapple", isSuperApple());
		return json.toString();
	}
	
	@Override
	public void updateFromJSON(JSONObject json) {
		super.updateFromJSON(json);
		this.isSuperApple = json.optBoolean("superapple");
	}
	
}
