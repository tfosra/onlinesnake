package entity;

import java.awt.Point;

import org.json.JSONObject;

public class PortalBlock extends Block {

	private int orientation;
	
	public PortalBlock() {
		super();
	}
	
	public PortalBlock(Point p) {
		this(p, ORIENTATION_R);
	}
	
	public PortalBlock(Point p, int orientation) {
		super(p, IBlock.PORTAL_BLOCK);
		this.orientation = orientation;
	}
	
	public int getOrientation() {
		return orientation;
	}
	
	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}
	
	public String toString() {
		String res = super.toString();
		JSONObject json = new JSONObject(res);
		json.put("orientation", getOrientation());
		return json.toString();
	}
	
	@Override
	public void updateFromJSON(JSONObject json) {
		super.updateFromJSON(json);
		this.orientation = json.optInt("orientation");
	}
	
}
