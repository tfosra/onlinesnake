package observation;

public interface Observer {

	void update(String message);

}
