package observation;

public interface Observable {
	
	void addObserver(Observer obs);
	
	void removeObservers();
	
	void notifyObservers(String message);

}
